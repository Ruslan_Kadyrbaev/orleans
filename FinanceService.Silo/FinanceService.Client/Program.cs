﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceService.Contracts;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Orleans;
using Orleans.Configuration;

namespace FinanceService.Client
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            Console.WriteLine("Client");
            var client = await CreateClient();
            var count = 0;
            //Console.WriteLine("Press Enter to start...");
            //Console.ReadLine();

            var tasks = new List<Task>();
            foreach (var i in Enumerable.Range(1, 100))
            {
                foreach (var x in Enumerable.Range(1, 20))
                {
                    tasks.Add(Work(client, i, x));
                }
            }

            await Task.WhenAll(tasks.ToArray());

            Console.WriteLine("Done");
            Console.WriteLine(count);
            Console.WriteLine("Press Enter to terminate...");
            Console.ReadKey();
            await client.Close();
        }

        public static async Task Work(IClusterClient client, int i, int x)
        {
            var payment = new MakePaymentRequest()
            {
                TransactionId = Guid.NewGuid(),
                Amount = new Amount()
                {
                    CurrencyCode = "643",
                    CurrencyRate = 1,
                    Value = 1000,
                }
            };

            var paymentGrain = client.GetGrain<IPaymentGrain>(payment.TransactionId);
            var response = await paymentGrain.MakePayment(payment);
            var paymentGrain2 = client.GetGrain<IPaymentGrain>(payment.TransactionId);
            var response2 = await paymentGrain.GetInfo(new PaymentInfoRequest()
            {
                Id = payment.TransactionId,
            });
            Console.WriteLine($"{i}:{x}: {JsonConvert.SerializeObject(new { response , response2 })}");
        }

        public static async Task<IClusterClient> CreateClient()
        {
            var builder = new ClientBuilder()
                .UseLocalhostClustering()
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "orleans-docker";
                    options.ServiceId = "FinanceService";
                })
                .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(IPaymentGrain).Assembly).WithReferences())
                .ConfigureLogging(logging => logging.AddConsole());

            var client = builder.Build();
            await client.Connect();
            return client;
        }
    }
}
