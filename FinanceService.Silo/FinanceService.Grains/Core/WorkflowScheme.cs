﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceService.Contracts;

namespace FinanceService.Grains.Core
{
    public class WorkflowScheme<TCommandResult>
        where TCommandResult : class, ICommandResult
    {
        public List<WorkflowNode> Nodes { get; set; } = new List<WorkflowNode>();
        public WorkflowNode Root { get; set; }
        private WorkflowNode _activeNode = null;

        public static WorkflowScheme<TCommandResult> New()
        {
            var flow = new WorkflowScheme<TCommandResult>();
            flow._activeNode = new WorkflowNode(name: "$Root", command: null, async: false);
            return flow;
        }

        public WorkflowScheme<TCommandResult> First(string name, Func<Task<TCommandResult>> command)
        {
            this._activeNode = new WorkflowNode(name, command: command, async: false);
            this.Nodes.Add(this._activeNode);
            this.Root = this._activeNode;
            return this;
        }

        public WorkflowScheme<TCommandResult> Next(string name, Func<Task<TCommandResult>> command)
        {
            var nextNode = new WorkflowNode(name, command: command, async: false);
            this._activeNode.NextNode = nextNode;
            this._activeNode = nextNode;
            this.Nodes.Add(this._activeNode);
            return this;
        }

        public WorkflowScheme<TCommandResult> NextAsync(string name, Func<Task<TCommandResult>> asyncCommand)
        {
            var nextNode = new WorkflowNode(name, command: asyncCommand, async: true);
            this._activeNode.NextNode = nextNode;
            this._activeNode = nextNode;
            this.Nodes.Add(this._activeNode);
            return this;
        }

        public WorkflowScheme<TCommandResult> Final(string name, Func<Task<TCommandResult>> command)
        {
            var lastNode = new WorkflowNode(name, command: command, async: false);
            this._activeNode.NextNode = lastNode;
            this._activeNode = lastNode;
            this.Nodes.Add(this._activeNode);
            return this;
        }

        public WorkflowScheme<TCommandResult> RetryOnError()
        {
            return this;
        }

        public WorkflowScheme<TCommandResult> OnError(Func<Task<TCommandResult>> command)
        {
            return this;
        }

        public WorkflowScheme<TCommandResult> OnTimeout(Func<Task<TCommandResult>> command)
        {
            return this;
        }

        public WorkflowScheme<TCommandResult> AtAll(Func<Task<TCommandResult>> command)
        {
            return this;
        }

        public class WorkflowNode
        {
            public Func<Task<TCommandResult>> Command { get; }
            public WorkflowNode NextNode { get; set; }
            public string Name { get; set; }
            public bool Async { get; set; }

            public WorkflowNode(string name, Func<Task<TCommandResult>> command, bool async)
            {
                Name = name;
                Command = command;
                Async = async;
            }
        }
    }
}
