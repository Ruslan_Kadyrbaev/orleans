﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FinanceService.Contracts;
using FinanceService.Contracts.Models;

namespace FinanceService.Grains.Core
{
    public class WorkflowProcessor<TCommandResult>
        where TCommandResult : class, ICommandResult
    {
        public async Task Next(IProcess<TCommandResult> process, WorkflowScheme<TCommandResult> scheme, Func<Task> onProcessChanged)
        {
            while (process.IsCompleted == false && process.IsSuspended == false)
            {
                var node = scheme.Nodes.Single(x => x.Name == process.CurrentNode);
                var commandResult = await node.Command();

                process.CurrentCommandResult = commandResult;
                process.IsSuspended = node.Async;
                process.CurrentNode = process.CurrentCommandResult.Success == true && process.IsSuspended == false ? node.NextNode?.Name ?? "" : node.Name;
                process.IsCompleted = process.CurrentCommandResult.Success == true && node.NextNode == null;

                process.Steps.Add(new ProcessNode<TCommandResult>()
                {
                    NodeName = node.Name,
                    IsSuspended = process.IsSuspended,
                    CommandResult = commandResult,
                    ActionDate = DateTime.Now,
                    Passed = commandResult.Success,
                });

                await onProcessChanged();

                if (process.CurrentCommandResult.Success == false)
                {
                    break;
                }
            }
        }

        public async Task Resume(TCommandResult commandResult, IProcess<TCommandResult> process, WorkflowScheme<TCommandResult> scheme, Func<Task> onProcessChanged)
        {
            if (process.IsSuspended == true)
            {
                var node = scheme.Nodes.Single(x => x.Name == process.CurrentNode);
                process.IsSuspended = false;
                process.CurrentNode = node.NextNode.Name;

                process.Steps.Add(new ProcessNode<TCommandResult>()
                {
                    NodeName = node.Name,
                    IsSuspended = process.IsSuspended,
                    CommandResult = commandResult,
                    ActionDate = DateTime.Now,
                    Passed = commandResult.Success,
                });
                await onProcessChanged();

                await this.Next(process, scheme, onProcessChanged);
            }
            else
            {
                await this.Next(process, scheme, onProcessChanged);
            }
        }
    }
}
