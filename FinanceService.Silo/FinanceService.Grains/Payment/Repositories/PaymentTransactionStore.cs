﻿using System;
using System.IO;
using System.Threading.Tasks;
using FinanceService.Contracts.Models;
using Newtonsoft.Json;

namespace FinanceService.Grains.Payment.Repositories
{
    public class PaymentTransactionStore
    {
        public async Task<PaymentTransaction> GetTransaction(Guid transactionId)
        {
            var fileName = $@"c:\temp\{transactionId}.json";
            if (File.Exists(fileName))
            {
                var json = File.ReadAllText(fileName);
                var transaction = JsonConvert.DeserializeObject<PaymentTransaction>(json);
                return await Task.FromResult(transaction);
            }

            return null;
        }

        public async Task SaveTransaction(PaymentTransaction transaction)
        {
            var fileName = $@"c:\temp\{transaction.TransactionId}.json";
            var json = JsonConvert.SerializeObject(transaction);
            File.WriteAllText(fileName, json);

            await Task.FromResult(true);
        }
    }
}
