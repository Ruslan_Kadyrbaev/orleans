﻿using System;
using System.IO;
using System.Threading.Tasks;
using FinanceService.Contracts.Models;
using Newtonsoft.Json;

namespace FinanceService.Grains.Payment.Repositories
{
    public class PaymentProcessStore
    {
        public async Task<PaymentProcess> GetProcess(Guid processId)
        {
            var fileName = $@"c:\temp\wf_{processId}.json";
            if (File.Exists(fileName))
            {
                var json = File.ReadAllText(fileName);
                var workflow = JsonConvert.DeserializeObject<PaymentProcess>(json);
                return await Task.FromResult(workflow);
            }
            return null;
        }

        public async Task SaveProcess(PaymentProcess process)
        {
            var fileName = $@"c:\temp\wf_{process.ProcessId}.json";
            var json = JsonConvert.SerializeObject(process);
            File.WriteAllText(fileName, json);
            await Task.FromResult(true);
        }
    }
}