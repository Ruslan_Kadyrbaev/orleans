﻿using System;
using System.Threading.Tasks;
using FinanceService.Contracts;
using Orleans;

namespace FinanceService.Grains.Payment.Internal
{
    public class EcommercePaymentAuthorizationGrain : Grain, IEcommercePaymentAuthorizationGrain
    {
        public async Task<AuthorizeResponse> Authorize(AuthorizeRequest request)
        {
            return await Task.FromResult(new AuthorizeResponse() { Success = true });
        }
    }

    public interface IEcommercePaymentAuthorizationGrain : IGrainWithGuidKey
    {
        Task<AuthorizeResponse> Authorize(AuthorizeRequest request);
    }

    public class AuthorizeRequest
    {
        public Guid TransactionId { get; set; }
        public Amount Amount { get; set; }
    }

    public class AuthorizeResponse
    {
        public bool Success { get; set; }
    }
}