﻿using System;
using System.Threading.Tasks;
using FinanceService.Contracts;
using Orleans;

namespace FinanceService.Grains.Payment.Internal
{
    public class EkassirPaymentTranslatorGrain : Grain, IEkassirPaymentTranslatorGrain
    {
        private Guid GrainTransactionId => this.GetPrimaryKey();

        public async Task<PaymentTranslatorResponse> TransferAsync(PaymentTranslatorRequest request, PaymentTranslatorResponseOptions options)
        {
            await SendResponse(options);
            return await Task.FromResult(new PaymentTranslatorResponse() { Success = true });
        }

        public async Task SendResponse(PaymentTranslatorResponseOptions options)
        {
            var streamProvider = GetStreamProvider(options.StreamProviderName);
            var stream = streamProvider.GetStream<PaymentTranslatorResponse>(this.GrainTransactionId, options.StreamNamespace);
            var task = stream.OnNextAsync(new PaymentTranslatorResponse() { Success = true });
            await Task.FromResult(true);
        }
    }

    public interface IEkassirPaymentTranslatorGrain : IGrainWithGuidKey
    {
        Task<PaymentTranslatorResponse> TransferAsync(PaymentTranslatorRequest request, PaymentTranslatorResponseOptions options);
    }

    public class PaymentTranslatorRequest
    {
        public Guid TransactionId { get; set; }
        public Amount Amount { get; set; }
    }

    public class PaymentTranslatorResponseOptions
    {
        public string StreamProviderName { get; set; }
        public string StreamNamespace { get; set; }
    }

    public class PaymentTranslatorResponse : ICommandResult
    {
        public bool Success { get; set; }
    }
}