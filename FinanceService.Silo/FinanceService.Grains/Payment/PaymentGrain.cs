﻿using System;
using System.Threading.Tasks;
using FinanceService.Contracts;
using FinanceService.Contracts.Models;
using FinanceService.Grains.Core;
using FinanceService.Grains.Payment.Internal;
using FinanceService.Grains.Payment.Repositories;
using Orleans;
using Orleans.Streams;

namespace FinanceService.Grains.Payment
{
    public class PaymentGrain : Grain, IPaymentGrain
    {
        private readonly WorkflowProcessor<MakePaymentResponse> _workflowProcessor = null;
        private readonly PaymentTransactionStore _paymentTransactionStore;
        private readonly PaymentProcessStore _processStore;
        private readonly WorkflowScheme<MakePaymentResponse> _scheme = null;

        private PaymentProcess _process = null;
        private PaymentTransaction _transaction = null;
        private StreamSubscriptionHandle<PaymentTranslatorResponse> _paymentTranslatorSubscription = null;

        private Guid GrainTransactionId => this.GetPrimaryKey();
        private Guid GrainProcessId => this.GetPrimaryKey();

        public PaymentGrain()
        {
            this._paymentTransactionStore = new PaymentTransactionStore();
            this._processStore = new PaymentProcessStore();
            this._workflowProcessor = new WorkflowProcessor<MakePaymentResponse>();

            this._scheme = WorkflowScheme<MakePaymentResponse>.New()
                .First("Authorize", this.Authorize)
                    .RetryOnError()
                    .OnError(this.AuthorizeOnError)
                    .OnTimeout(this.AuthorizeOnTimeout)
                    .AtAll(this.AuthorizeAtAll)
                .NextAsync("Transfer", () => this.TransferAsync())
                    .RetryOnError()
                    .OnError(this.TransferOnError)
                    .OnTimeout(this.TransferOnTimeout)
                    .AtAll(this.TransferAtAll)
                .Final("Success", this.Success);
        }

        public override async Task OnActivateAsync()
        {
            this._transaction = await this._paymentTransactionStore.GetTransaction(this.GrainTransactionId);
            this._process = await this._processStore.GetProcess(this.GrainProcessId);
            await base.OnActivateAsync();
        }

        public async Task<MakePaymentResponse> MakePayment(MakePaymentRequest request)
        {
            await this.CreateTransaction(request);

            await this._workflowProcessor.Next(this._process, this._scheme, onProcessChanged: async () =>
            {
                await this._processStore.SaveProcess(this._process);
            });

            return this._process.CurrentCommandResult;
        }

        public async Task<PaymentInfoResponse> GetInfo(PaymentInfoRequest request)
        {
            return await Task.FromResult(new PaymentInfoResponse()
            {
                Success = true,
                PaymentTransaction = this._transaction,
            });
        }

        private async Task CreateTransaction(MakePaymentRequest request)
        {
            this.GuardMakePayment(request);

            this._transaction = new PaymentTransaction()
            {
                TransactionId = request.TransactionId,
                Amount = request.Amount,
            };
            await this._paymentTransactionStore.SaveTransaction(this._transaction);

            this._process = new PaymentProcess()
            {
                ProcessId = request.TransactionId,
                CurrentNode = this._scheme.Root.Name,
            };
            await this._processStore.SaveProcess(this._process);
        }

        private async Task<MakePaymentResponse> Authorize()
        {
            var holder = this.GrainFactory.GetGrain<IEcommercePaymentAuthorizationGrain>(_transaction.TransactionId);
            var holdResult = await holder.Authorize(new AuthorizeRequest()
            {
                TransactionId = this._transaction.TransactionId,
                Amount = this._transaction.Amount,
            });

            return MakePaymentResponse.Ok();
        }

        private async Task<MakePaymentResponse> TransferAsync(string streamProviderName = "SMSProvider", string streamNamespace = "PaymentTransfer")
        {
            var streamProvider = GetStreamProvider(streamProviderName);
            var stream = streamProvider.GetStream<PaymentTranslatorResponse>(this._transaction.TransactionId, streamNamespace);
            this._paymentTranslatorSubscription = await stream.SubscribeAsync(async (response, token) => await this.TransferHandler(response));

            var translator = this.GrainFactory.GetGrain<IEkassirPaymentTranslatorGrain>(_transaction.TransactionId);
            var transferRequest = new PaymentTranslatorRequest() { TransactionId = this._transaction.TransactionId, Amount = this._transaction.Amount, };
            var transferOptions = new PaymentTranslatorResponseOptions() { StreamProviderName = streamProviderName, StreamNamespace = streamNamespace, };
            var transferResult = await translator.TransferAsync(transferRequest, transferOptions);

            return MakePaymentResponse.Ok();
        }

        private async Task<MakePaymentResponse> TransferHandler(PaymentTranslatorResponse response)
        {
            await this._paymentTranslatorSubscription.UnsubscribeAsync();
            this._paymentTranslatorSubscription = null;

            await this._workflowProcessor.Resume(new MakePaymentResponse() {Success = true }, this._process, this._scheme, onProcessChanged: async () =>
            {
                await this._processStore.SaveProcess(this._process);
            });

            return this._process.CurrentCommandResult;
        }

        private async Task<MakePaymentResponse> Success()
        {
            return await Task.FromResult(MakePaymentResponse.Ok());
        }

        private void GuardMakePayment(MakePaymentRequest request)
        {
            if (this._process != null || this._transaction != null)
            {
                throw new NotSupportedException("Payment was already made");
            }

            if (this.GrainTransactionId != request.TransactionId || this.GrainProcessId != request.TransactionId)
            {
                throw new NotSupportedException("IDs are not equal");
            }
        }

        private Task<MakePaymentResponse> TransferAtAll()
        {
            throw new NotImplementedException();
        }

        private Task<MakePaymentResponse> TransferOnTimeout()
        {
            throw new NotImplementedException();
        }

        private Task<MakePaymentResponse> TransferOnError()
        {
            throw new NotImplementedException();
        }

        private Task<MakePaymentResponse> AuthorizeAtAll()
        {
            throw new NotImplementedException();
        }

        private Task<MakePaymentResponse> AuthorizeOnTimeout()
        {
            throw new NotImplementedException();
        }

        private Task<MakePaymentResponse> AuthorizeOnError()
        {
            throw new NotImplementedException();
        }
    }
}
