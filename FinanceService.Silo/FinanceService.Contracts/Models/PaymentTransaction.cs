﻿using System;

namespace FinanceService.Contracts.Models
{
    public class PaymentTransaction
    {
        public Guid TransactionId { get; set; }
        public Amount Amount { get; set; }
    }
}