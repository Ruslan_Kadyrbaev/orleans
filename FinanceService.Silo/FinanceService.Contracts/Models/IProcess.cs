﻿using System;
using System.Collections.Generic;

namespace FinanceService.Contracts.Models
{
    public interface IProcess<TCommandResult>
        where TCommandResult : class, ICommandResult
    {
        Guid ProcessId { get; set; }
        bool IsCompleted { get; set; }
        bool IsSuspended { get; set; }
        string CurrentNode { get; set; }
        TCommandResult CurrentCommandResult { get; set; }
        List<ProcessNode<TCommandResult>> Steps { get; }
    }
}