﻿using System;

namespace FinanceService.Contracts.Models
{
    public class ProcessNode<TCommandResult>
        where TCommandResult : class, ICommandResult
    {
        public string NodeName { get; set; }
        public bool IsSuspended { get; set; }
        public DateTime ActionDate { get; set; }
        public bool Passed { get; set; }
        public TCommandResult CommandResult { get; set; }
    }
}