﻿using System;
using System.Collections.Generic;

namespace FinanceService.Contracts.Models
{
    public class PaymentProcess : IProcess<MakePaymentResponse>
    {
        public Guid ProcessId { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsSuspended { get; set; }
        public string CurrentNode { get; set; }
        public MakePaymentResponse CurrentCommandResult { get; set; }
        public List<ProcessNode<MakePaymentResponse>> Steps { get; set; } = new List<ProcessNode<MakePaymentResponse>>();
    }
}