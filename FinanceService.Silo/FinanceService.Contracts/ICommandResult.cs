﻿namespace FinanceService.Contracts
{
    public interface ICommandResult
    {
        bool Success { get; set; }
    }
}
