﻿using System;
using System.Threading.Tasks;
using FinanceService.Contracts.Models;
using Orleans;

namespace FinanceService.Contracts
{
    public interface IPaymentGrain : IGrainWithGuidKey
    {
        Task<MakePaymentResponse> MakePayment(MakePaymentRequest request);
        Task<PaymentInfoResponse> GetInfo(PaymentInfoRequest request);
    }

    public class MakePaymentRequest
    {
        public Guid TransactionId { get; set; }
        public Amount Amount { get; set; }
    }

    public class MakePaymentResponse : ICommandResult
    {
        public bool Success { get; set; }

        public static MakePaymentResponse Ok()
        {
            return new MakePaymentResponse()
            {
                Success = true,
            };
        }
    }

    public class PaymentInfoRequest
    {
        public Guid Id { get; set; }
    }

    public class PaymentInfoResponse
    {
        public bool Success { get; set; }
        public PaymentTransaction PaymentTransaction { get; set; }
    }

    public struct Amount
    {
        public decimal Value { get; set; }
        public string CurrencyCode { get; set; }
        public decimal CurrencyRate { get; set; }
    }
}
