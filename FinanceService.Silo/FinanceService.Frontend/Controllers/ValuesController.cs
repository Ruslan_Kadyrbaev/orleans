﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceService.Contracts;
using Microsoft.AspNetCore.Mvc;
using Orleans;

namespace FinanceService.Frontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IClusterClient _clusterClient;

        public ValuesController(IClusterClient clusterClient)
        {
            _clusterClient = clusterClient;
        }

        // GET api/values
        [HttpGet]
        public async Task<object> Get()
        {
            var payment = new MakePaymentRequest()
            {
                TransactionId = Guid.NewGuid(),
                Amount = new Amount()
                {
                    CurrencyCode = "643",
                    CurrencyRate = 1,
                    Value = 1000,
                }
            };

            var paymentGrain = _clusterClient.GetGrain<IPaymentGrain>(payment.TransactionId);
            var response = await paymentGrain.MakePayment(payment);
            //var transactionInfo = await paymentGrain.GetInfo(new PaymentInfoRequest() { Id = payment.TransactionId });
            return response;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
