﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceService.Contracts;
using Microsoft.Extensions.Configuration;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.TestingHost;
using Xunit;

namespace FinanceService.IntegrationTests
{
    public class ClusterFixture : IDisposable
    {
        public ClusterFixture()
        {
            var builder = new TestClusterBuilder(1);
            builder.AddSiloBuilderConfigurator<SiloHostConfigurator>();
            var host = builder.Build();
            host.Deploy();
            this.Cluster = host;
        }

        public void Dispose()
        {
            this.Cluster.StopAllSilos();
        }

        public TestCluster Cluster { get; private set; }
    }

    public class SiloHostConfigurator : ISiloBuilderConfigurator
    {
        public void Configure(ISiloHostBuilder hostBuilder)
        {
            hostBuilder
                .AddMemoryGrainStorage("MemoryStore")
                .AddMemoryGrainStorage("PubSubStore")
                .AddSimpleMessageStreamProvider("SMSProvider")
                .Configure<SchedulingOptions>(options => options.AllowCallChainReentrancy = false)
                ;
        }
    }

    [CollectionDefinition(ClusterCollection.Name)]
    public class ClusterCollection : ICollectionFixture<ClusterFixture>
    {
        public const string Name = "ClusterCollection";
    }

    [Collection(ClusterCollection.Name)]
    public class PaymentGrainTests
    {
        private readonly TestCluster _cluster;

        public PaymentGrainTests(ClusterFixture fixture)
        {
            _cluster = fixture.Cluster;
        }

        [Fact]
        public async Task SaysHelloCorrectly()
        {
            var payment = new MakePaymentRequest()
            {
                TransactionId = Guid.NewGuid(),
                Amount = new Amount()
                {
                    CurrencyCode = "643",
                    CurrencyRate = 1,
                    Value = 1000,
                }
            };
            var grain = this._cluster.GrainFactory.GetGrain<IPaymentGrain>(payment.TransactionId);
            var response = await grain.MakePayment(payment);
            Assert.NotNull(response);
        }
    }
}
